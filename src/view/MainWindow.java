package view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.util.Callback;
import model.Livre;
import modelview.LivreVM;
import modelview.PapillonVM;
import stub.Stub;

public class MainWindow {
    @FXML
    public ListView<modelview.PapillonVM> lvPapillonVM;
    @FXML
    private TextField textFieldNom;

    private LivreVM livreVM;

    public void initialize(){
        livreVM = new LivreVM();

        lvPapillonVM.itemsProperty().bind(livreVM.lesPapillonVMProperty());

        lvPapillonVM.getSelectionModel().selectedItemProperty().addListener((__, oldValue, newValue) -> {
            if(oldValue != null){
                textFieldNom.textProperty().unbindBidirectional(oldValue.nomProperty());
            }
            if(newValue != null){
                textFieldNom.textProperty().bindBidirectional(newValue.nomProperty());
            }
        });

        lvPapillonVM.setCellFactory(param -> new ListCell<>(){
            @Override
            protected void updateItem(PapillonVM item, boolean empty) {
                super.updateItem(item, empty);
                if(empty){
                    textProperty().unbind();
                    setText("");
                } else {
                    textProperty().bind(item.nomProperty());
                }
            }
        });
    }

    @FXML
    private void add() {
        livreVM.addPapillonVM();
    }

    public Livre getLivre(){
        return livreVM.getLivre();
    }
}
