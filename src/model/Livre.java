package model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class Livre implements Serializable {
    private List<Papillon> collections;
    private int index = 0;
    public static final String INDEX_PROPERTY = "index";
    public static final String COLLECTIONS_PROPERTY = "collections";

    private transient PropertyChangeSupport support;

    public Livre(List<Papillon> collections) {
        this.collections = collections;
        support = new PropertyChangeSupport(this);
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) throws Exception {
        if(index >= 0 && index < collections.size()){
            this.index = index;
        }
        else {
            throw new Exception("ERROR INDEX");
        }
    }

    public void nextIndex() throws Exception {
        setIndex(index+1);
        getSupport().firePropertyChange(INDEX_PROPERTY,index-1,index);
    }

    public void previusIndex() throws Exception {
        setIndex(index-1);
        getSupport().firePropertyChange(INDEX_PROPERTY,index+1,index);
    }

    public void addObservateur(PropertyChangeListener listener){
        getSupport().addPropertyChangeListener(listener);
    }

    public Papillon getCurrentPapillon(){
        return collections.get(index);
    }

    public List<Papillon> getCollections(){
        return Collections.unmodifiableList(collections);
    }

    public void addPapillon() {
        collections.add(new Papillon("NOUVEAU PAPILLON"));
        getSupport().fireIndexedPropertyChange(COLLECTIONS_PROPERTY,collections.size()-1,null,collections.get(collections.size()-1));
    }

    private PropertyChangeSupport getSupport(){
        if(support == null){
            support = new PropertyChangeSupport(this);
        }
        return support;
    }
}
