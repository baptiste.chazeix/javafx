package model;

import stub.Stub;

import java.io.*;

public class LivreIO  implements IO{
    @Override
    public void save(Object livre){
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("data.bin"))) {
            oos.writeObject(livre);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Livre load(){
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("data.bin"))){
            return (Livre)ois.readObject();
        } catch (Exception e) {
            return Stub.creerLivre();
        }
    }
}
