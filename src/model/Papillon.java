package model;

import javafx.beans.property.StringProperty;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

public class Papillon implements Serializable {
    private String nom;
    public static final String NOM_PROPERTY = "nom";

    private transient PropertyChangeSupport support;

    public Papillon(String nom) {
        this.nom = nom;
        support = new PropertyChangeSupport(this);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        String old = this.nom;
        this.nom = nom;
        getSupport().firePropertyChange(NOM_PROPERTY,old,nom);
    }

    public void addObservateur(PropertyChangeListener listener){
        getSupport().addPropertyChangeListener(listener);
    }

    private PropertyChangeSupport getSupport(){
        if(support == null){
            support = new PropertyChangeSupport(this);
        }
        return support;
    }
}
