package model;

public interface IO<T>{
    void save(T value);
    T load();
}
