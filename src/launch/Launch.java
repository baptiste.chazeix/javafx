package launch;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.LivreIO;
import view.MainWindow;

import java.net.URI;

public class Launch extends Application {

    private MainWindow mw;

    @Override
    public void start(Stage primaryStage) throws Exception {
        mw = new MainWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/MainWIndow.fxml"));
        loader.setController(mw);
        Parent parent = loader.load();
        primaryStage.setScene(new Scene(parent));
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        new LivreIO().save(mw.getLivre());
        super.stop();
    }
}
