package stub;

import model.Livre;
import model.Papillon;

import java.util.ArrayList;
import java.util.List;

public class Stub {
    public static Papillon creerPapillon(){
        return new Papillon("NOUVEAU PAPILLON");
    }

    public static Livre creerLivre(){
        List<Papillon> p = new ArrayList<>();
        p.add(new Papillon("debut"));
        p.add(new Papillon("1"));
        p.add(new Papillon("2"));
        p.add(new Papillon("fin"));
        return new Livre(p);
    }
}
