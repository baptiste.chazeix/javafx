package modelview;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Livre;
import model.LivreIO;
import model.Papillon;
import stub.Stub;

import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class LivreVM implements PropertyChangeListener {
    private Livre model;
    private PapillonVM papillonVM;

    private ObservableList<PapillonVM> lesPapillonVMObs = FXCollections.observableArrayList();

    private ListProperty<PapillonVM> lesPapillonVM = new SimpleListProperty<>(lesPapillonVMObs);
    public ObservableList<PapillonVM> getLesPapillonVM() {return FXCollections.unmodifiableObservableList(lesPapillonVMObs);}
    public ReadOnlyListProperty<PapillonVM> lesPapillonVMProperty() {return lesPapillonVM;}
    private void setLesPapillonVM(ObservableList<PapillonVM> lesPapillonVM) {this.lesPapillonVM.set(lesPapillonVM);}

    private IntegerProperty index = new SimpleIntegerProperty();
    public int getIndex() {return index.get();}
    public IntegerProperty indexProperty() {return index;}
    private void setIndex(int index) {this.index.set(index);}

    public PapillonVM getPapillonVM() {
        return papillonVM;
    }

    public LivreVM(Livre livre) {
        model = livre;
        rebuildPapillonVM();
        index.addListener((observable, oldValue, newValue) -> {
            try{
                model.setIndex((int)newValue);
            }
            catch (Exception e){
                e.printStackTrace();
            }
            rebuildPapillonVM();
        });
        model.addObservateur(this);
        setIndex(model.getIndex());
    }

    public LivreVM(){
        model = new LivreIO().load();
        model.addObservateur(this);
        model.getCollections().forEach(papillon -> {
            lesPapillonVMObs.add(new PapillonVM(papillon));
        });
    }

    private  void rebuildPapillonVM(){
        papillonVM = new PapillonVM(model.getCurrentPapillon());
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if(propertyChangeEvent.getPropertyName().equals(Livre.INDEX_PROPERTY)){
            setIndex((int)propertyChangeEvent.getNewValue());
        }
        if(propertyChangeEvent.getPropertyName().equals(Livre.COLLECTIONS_PROPERTY)){
            lesPapillonVMObs.add(((IndexedPropertyChangeEvent)propertyChangeEvent).getIndex(),new PapillonVM((Papillon)propertyChangeEvent.getNewValue()));
        }
    }

    public void addPapillonVM() {
        model.addPapillon();
    }

    public Livre getLivre(){
        return model;
    }
}
