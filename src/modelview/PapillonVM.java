package modelview;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import model.Papillon;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class PapillonVM implements PropertyChangeListener {

    private Papillon model;

    private StringProperty nom = new SimpleStringProperty();
    public String getNom() {return nom.get();}
    public StringProperty nomProperty() {return nom;}
    private void setNom(String nom) {this.nom.set(nom);}

    public PapillonVM(Papillon papillon) {
        model = papillon;

        nom.addListener((observable, oldValue, newValue) -> {
            model.setNom(newValue);
        });
        model.addObservateur(this);

        setNom(papillon.getNom());
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if(propertyChangeEvent.getPropertyName().equals(Papillon.NOM_PROPERTY)){
            setNom((String)propertyChangeEvent.getNewValue());
        }
    }
}
